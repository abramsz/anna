# Anna

Anna is a web scaffold for generating a new go web application.


Usage
-------------
`
$ anna -n gitea.com/abramsz/hello
`

Build
-------------
`
$ task build
`

Installation
-------------
`
$ go get gitea.com/abramsz/anna
`

Development Prerequisites
-------------
- Go 1.14 or later.
- The following binary must be in PATH 
    - https://github.com/jteeuwen/go-bindata
    - https://github.com/go-task/task

Todo
-------------
- authentication support via middleware
- subrouter support
- orm based on code generator
- jwt support
- validation
