package main

import (
	"os"
	"path/filepath"
	"testing"
)

func TestTouch(t *testing.T) {
	files := []string{
		"pkg/hello/hello.go",
		"hello.go",
	}

	for _, p := range files {
		dir := filepath.Dir(p)
		if dir == "." {
			continue
		} else {
			os.MkdirAll(dir, 0700)
		}
	}

	for _, file := range files {
		touch(file)
	}
}

func TestParseTemplateName(t *testing.T) {
	templates := []string{
		"templates/cmd/{{.ModName}}/config/config.go.html",
	}

	data := Data{
		Mod:     "github.com/abramsz/anna",
		ModName: "anna",
	}

	for _, templ := range templates {
		target, err := parseTargetName(templ, data)
		if err != nil {
			t.Fail()
		}
		t.Log(target)
	}

}
