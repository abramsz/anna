//go:generate go-bindata -pkg asset -o asset/asset.go templates/...
package main

import (
	"bytes"
	"flag"
	"github.com/abramsz/anna/asset"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

/**
anna -n github.com/abramsz/alisa
*/

var mod = flag.String("n", "github.com/abramsz/alisa", "the mod name of the project")
var isOverride = flag.Bool("y", false, "when project existed, override it or not")
var cwd, err = filepath.Abs(filepath.Dir(os.Args[0]))

type Data struct {
	Mod     string
	ModName string
}

func main() {
	flag.Parse()

	err = createMod(*mod)

	if err != nil {
		log.Fatal(err)
	}
}

func createMod(mod string) error {
	modName := parseMod(mod)

	err := os.Mkdir(modName, 0700)

	if !*isOverride && os.IsExist(err) {
		return err
	}

	err = nil

	data := Data{
		Mod:     mod,
		ModName: modName,
	}

	for _, tmpl := range asset.AssetNames() {
		renderTarget(modName, tmpl, data)
	}

	return err
}

func renderTarget(mod, templateName string, data interface{}) error {
	targetName := strings.TrimSuffix(templateName, ".html")
	targetName = strings.TrimPrefix(targetName, "templates/")

	asset, err := asset.Asset(templateName)

	if err != nil {
		log.Fatal(err)
	}

	t := template.Must(template.New(targetName).Parse(string(asset)))

	parsedTargetName, err := parseTargetName(targetName, data)

	if err != nil {
		log.Fatal(err)
	}

	f, err := touch(mod + "/" + parsedTargetName)
	defer f.Close()

	if err != nil {
		log.Fatal(err)
	}

	err = t.Execute(f, data)

	return err
}

func parseTargetName(templateName string, data interface{}) (string, error) {
	t := template.Must(template.New(templateName).Parse(templateName))

	if err != nil {
		log.Fatal(err)
	}

	b := bytes.NewBuffer(make([]byte, 0))

	err = t.Execute(b, data)

	if err != nil {
		return "", err
	}

	return b.String(), nil
}

func touch(filename string) (*os.File, error) {
	dir := filepath.Dir(filename)
	if dir != "." {
		os.MkdirAll(dir, 0700)
	}

	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0700)

	return f, err
}

func parseMod(modName string) string {
	mods := strings.Split(modName, "/")
	if len(mods) > 1 {
		return mods[len(mods)-1]
	}
	return modName
}
